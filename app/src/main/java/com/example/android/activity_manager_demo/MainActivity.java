package com.example.android.activity_manager_demo;

import android.app.ActivityManager;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    Button mBtn;
    TextView outPut;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mBtn=findViewById(R.id.btnGetList);

        mBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getList();
            }
        });
    outPut=findViewById(R.id.output);
    }

    private void getList() {
        ActivityManager activityManager= (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);

        List<ActivityManager.RunningServiceInfo> runService=activityManager.getRunningServices
                (Integer.MAX_VALUE);

        List<ActivityManager.RunningAppProcessInfo> runApp=activityManager.getRunningAppProcesses();

        List<ActivityManager.RunningTaskInfo> runTasks=activityManager.getRunningTasks(Integer
                .MAX_VALUE);

        List<ActivityManager.RecentTaskInfo> recentTaskInfos=activityManager.getRecentTasks
                (Integer.MAX_VALUE,ActivityManager.RECENT_WITH_EXCLUDED);

        outPut.setText("Running Task List");

        for(ActivityManager.RunningTaskInfo task:runTasks){
            outPut.append("\n"+ task.baseActivity.getPackageName()+"("+ task.baseActivity
                    .getShortClassName()+")");
        }
        outPut.append("\n\nRunning Services list:");
        for (ActivityManager.RunningServiceInfo task : runService) {
            outPut.append("\n"+ task.service.getPackageName());
        }

        outPut.append("\n\nAppProcess list:");
        for (ActivityManager.RunningAppProcessInfo task : runApp) {
            //output.append("\n"+ task.importanceReasonComponent.getPackageName());
            outPut.append("\n"+ task.processName);
        }

        outPut.append("\n\nRecent Task list:");
        for (ActivityManager.RecentTaskInfo task : recentTaskInfos) {
            //output.append("\n"+ task.origActivity.getPackageName());
            outPut.append("\n"+ task.baseIntent.getComponent().getPackageName());
        }

    }
}
